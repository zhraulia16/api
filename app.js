const express = require("express")
const users = require("./db/users.json")

const app = express()

app.use(express.json())

// app.get("/api/v1/users", (req, res) => {
//     res.send(users)
// })

app.get("/api/v1/users/:id", (req, res) => {
    const user = users.find(i => i.id === req.params.id)
    
    res.status(200).json(user)
})

app.post("/api/v1/users", (req, res) => {
    const { email, first_name, last_name, avatar} = req.body

    const id = users[users.length - 1].id + 1
    const user = {
        id, email, first_name, last_name, avatar
    }

    users.push(user)

    res.status(201).json(user)
})

app.put("/api/v1/users/:id", (req, res) => {
    let user = users.find(i => i.id === +req.params.id)
    
    const params = {
        "email": req.body.email,
        "first_name": req.body.first_name,
        "last_name": req.body.last_name,
        "avatar": req.body.avatar
    }
    
    user = { ...user, ...params }

    user = users.map(i => i.id === user.id? user : i)
   
    res.status(200).json(user)
})


app.delete("/api/v1/users/:id", (req, res) => {
    user = users.filter(i => i.id === +req.params.id)
    res.status(200).json({
        message: `Post dengan is ${req.params.id} sudah berhasil dihapus!`
    })
})

app.listen(3000, () => {
    console.log(`Example app listening at http://localhost:3000`)
})